
sealed abstract class Vehicle[A] {}

case class Car[T](cv:Double) extends Vehicle[T] {}

case class Motobike[T](cv:Double, wheel:Int) extends Vehicle[T] {}

case class Boat[T](name:String, length:Double) extends Vehicle[T] {}